# letsgoING ArduBlock Arduino IDE - Mac
Um direkt mit dem Programmieren loslegen zu können, haben wir Dir für alle gängigen Betriebssysteme eine Arduino IDE vorbereitet.
- [Windows](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-win)
- [Mac](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-mac)
- [Linux](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-lin)

## Fortschritte dieser Version
- einen Simulator für alle Grundlagenmodule
- in der Größe verstellbares Menü
- ein Papierkorb für nicht verwendetet Blöcke
- allgemeine Verbesserungen und Fehlerbehebungen 

## Download
Über den Button auf der rechten Seite kannst Du den Download starten. Wähle einfach ein passendes Dateiformat und der Download startet von selbst.

![](DownloadButton.png) 

Nach dem Download die Datei entpacken und in den Ordner wechseln.
Anschließend den Punkt Setup ausführen.

## Setup
1. Das .zip-File in ein lokales Verzeichnis entpacken
2. Die Arduino IDE einmalig starten und wieder beenden (vollständig - Anwendung darf nicht mehr im Schnellstartmenü sein)
3. Mit Rechtsklick (oder 2 Fingerklick) den Inhalt der App anzeigen lassen
4. Zum Ordner Contents/Java/tools navigieren
5. In den tools Order den Ordner ArduBlockTool kopieren oder bewegen

## Entwicklung
Damit unsere Entwicklungen schnellstmöglich zum Einsatz kommen können findet ihr die aktuelle Version [hier](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-mac/-/tree/dev/).
Aktuell haben wir folgende Punkte eingearbeitet:

- einen Simulator für alle Grundlagenmodule
- in der Größe verstellbares Menü
- allgemeine Verbesserungen und Fehlerbehebungen 


Viel Spaß,

Euer letsgoING-Team
